$(function () {
    $("#form").validate({
        rules:{
            email:{
                required: true,
                email: true
            },

            rut:{
               required: true,
               rut: true
                
            },

            nombreCompleto:{
                required: true,
                
            },

            fecNac:{
                required: true,
                date: true,
                fecNac : true
                
            },

            telContac:{
                number: true,
                
            }
        },
        messages: {
            email: {
                required: "Su email es requerido",
                email: "Formato de email no es válido."      
            },

            rut: {
                required: "Su Rut es requerido",
                rut: "¡Rut no valido! Favor ingrese rut correcto."
            },

            nombreCompleto: {
                required: "Su nombre completo es requerido "
            },

            fecNac: {
                required: "Su fecha de nacimiento es requerido "
            },

            telContac: {
                number: "Campo sólo admite numeros"
            }


        }
    });

/*llena cbxRegiones*/
var apiRegiones = new XMLHttpRequest();
//pregunta por el tipo de api
apiRegiones.responseType = 'json';
//pregunta por la url de la api
apiRegiones.open('GET', 'https://apis.digital.gob.cl/dpa/regiones/', true);
// si tiene respuesta hace lo siguiente ...
apiRegiones.onload  = function() {
    // var region es igual a la respuesta de la api, lo mismo que sale al cargar la url
   var varRegiones = apiRegiones.response;
   //recorre la respueta
   for(i in varRegiones){
       //por cada elemento dentro del objeto varRegiones
       //imprime un option dentro de #cbxRegion
       $("#cbxRegion").append('<option value="'+varRegiones[i].codigo+'">'+varRegiones[i].nombre+'</option>');
   }
}
//no envia respuesta a la api
apiRegiones.send(null);

// cada vez que la cbxRegion cambie (que seleccionen una opcion)
$('#cbxRegion').on('change', function(){
    //captura el value (val()) de la opcion escogida
    var regionSelected = $('#cbxRegion option:selected').val();
    //remueve los option que tenga el select
    $('#cbxCiudad').children().remove();
    //agraga un option que dice que escoga la comuna
    $("#cbxCiudad").append('<option value="null" selected disabled>Escoge una comuna</option>');
    //si la opcion seleccionada en #cbxRegion no es null
    if( regionSelected != 'null'){
        //url de la api
        var apiCiudadURL = 'https://apis.digital.gob.cl/dpa/regiones/'+regionSelected+'/comunas'
        //realiza un nuevo llamado a la api pero a las ciudadeds correspondiente de la region
        var apiCiudad = new XMLHttpRequest();
        apiCiudad.responseType = 'json';
        apiCiudad.open('GET', apiCiudadURL , true);
        apiCiudad.onload  = function(){
            var varCiudad = apiCiudad.response;
            for(i in varCiudad){
                //por cada ciudad imprime un option en #cbxCiudad
                $("#cbxCiudad").append('<option value="'+varCiudad[i].codigo+'">'+varCiudad[i].nombre+'</option>');
            }
        }
        //no envia resputa a la api
        apiCiudad.send(null);
    }
});// fin funcion on change
 
    var perros = {
        bigotes:{
            peso: "5kg",
            tamano: "medio",
            personalidad: "juguetón"
        },
        chocolate:{
            peso: "12kg",
            tamano: "grande",
            personalidad: "como tú"
        },
        luna:{
            peso: "2kg",
            tamano: "medio",
            personalidad: "juguetón"
        },
        maya:{
            peso: "6kg",
            tamano: "medio",
            personalidad: "alegre"
        },
        oso:{
            peso: "6kg",
            tamano: "Grande",
            personalidad: "jugueton"
        },
        pexel:{
            peso: "3kg",
            tamano: "medio",
            personalidad: "jugueton"
        },
        wifi:{
            peso: "1kg",
            tamano: "medio",
            personalidad: "jugueton"
        }
    }

    $('.pop').on("click", function(){
        var dataPerro = $(this).data('perro');
        var peso = perros[dataPerro].peso;
        var size = perros[dataPerro].tamano;
        var personalidad = perros[dataPerro].personalidad;
        var img  = $(this).children("source").attr('srcset');
        var divPop = [
                '<div class="pop-img" style="background-image: url('+img+');"></div>'+
                '<div class="info"><h2 class="title">'+dataPerro+'</h2>'+
                '<p> peso: '+peso+'</p>'+
                '<p> tamaño:'+size+'</p>'+
                '<p> personalidad: '+personalidad+'</p></div>'
            ];
        $('.pop-panel').addClass("active");
        $('.pop-content').append(divPop);
    });
    $('.btn-pop-close').on('click', function(){
        $('.pop-panel').removeClass("active");
        $('.pop-content').empty();
    })
    

    /***************
     * menu
    ***************/
    $(".menu-open").on('click', function(){
        //toggleClass agrega la clase cuando esta no esta en el elemento, 
        //si la clase esta la elimina
        $('.menu').toggleClass('active');
    });
    
    
    
    /*************
     * Valida Fecha Nacimiento anterior al 2001
     * ***************/
     
     function ValidaFecNac(campo) {

        if (parseInt(campo.substring(0,4)) >= 2001){
            return false;
        
        }else{ return true;}
       
        
    }
    
    
    
    /****************
     *Metodo RUT
     ***************/
    //Se añade validacion de Rut
    //Se extiende la funcianlidad del Metodo Validate añadiendo el Metodo RUT

    function validaRut(campo){
        if ( campo.length == 0 ){ return false; }
        if ( campo.length < 8 ){ return false; }
    
        campo = campo.replace('-','')
        campo = campo.replace(/\./g,'')

        if ( campo.length > 9 ){ return false; }
    
        var suma = 0;
        var caracteres = "1234567890kK";
        var contador = 0;    
        for (var i=0; i < campo.length; i++){
            u = campo.substring(i, i + 1);
            if (caracteres.indexOf(u) != -1)
            contador ++;
        }
        if ( contador==0 ) { return false }
        
        var rut = campo.substring(0,campo.length-1)
        var drut = campo.substring( campo.length-1 )
        var dvr = '0';
        var mul = 2;
        
        for (i= rut.length -1 ; i >= 0; i--) {
            suma = suma + rut.charAt(i) * mul
                    if (mul == 7) 	mul = 2
                    else	mul++
        }
        res = suma % 11
        if (res==1)		dvr = 'k'
                    else if (res==0) dvr = '0'
        else {
            dvi = 11-res
            dvr = dvi + ""
        }
        if ( dvr != drut.toLowerCase() ) { return false; }
        else { return true; }
    }
    
    /* La siguiente instrucción extiende las capacidades de jquery.validate() para que
        admita el método RUT, por ejemplo:
    
    $('form').validate({
        rules : { rut : { required:true, rut:true} } ,
        messages : { rut : { required:'Escriba el rut', rut:'Revise que esté bien escrito'} }
    })
    // Nota: el meesage:rut sobrescribe la definición del mensaje de más abajo
    */
    // comentar si jquery.Validate no se está usando
    jQuery.validator.addMethod("rut", function(value, element) { 
            return this.optional(element) || validaRut(value); 
    }, "¡Rut no valido! Favor ingrese rut correcto.");
    
    
    //Añade a FuNCION Validate
    jQuery.validator.addMethod("fecNac", function(value, element) { 
        return this.optional(element) || ValidaFecNac(value); 
    }, "Fecha Nacimiento debe ser anterior al 2001");

});